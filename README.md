# Magento2-Advanced-Login

Adds login with other attributes such as taxvat.
pt-BR - set-up CPF / CNPJ automatic login 

This package uses semaio/magento2-advancedlogin as base. As there has not been any update for years we preferred to start with this new package.

## Features

### Enhanced login process

By default, Magento only allows login via email and password.

This module adds support to:

* Login only via email
* Login only via customer attribute
* Login via customer attribute or email


## Support

For support / bug report please send email to suporte@gum.net.br

## Contribution
(c) 2016 Rouven Alexander Rieker

## Copyright
(c) 2020 Gustavo Ulyssea (https://gum.net.br) 